#pragma once

#ifndef AVLTREE_H
#define AVLTREE_H
#include <memory>
#include <string>
#include <algorithm>
#include "Exceptions.h"
#include <iostream>

#define COUNT 5


using std::shared_ptr;

template<class C>
class AVLNode {
public:
	int key;
	C data;
	shared_ptr<AVLNode> left;
	shared_ptr<AVLNode> right;
	shared_ptr<AVLNode> father;
	AVLNode(const int& key, const C& data, shared_ptr<AVLNode> father) :
		key(key), data(data), left(nullptr), right(nullptr), father(father) {};
};

template<class T>
class AVLTree {
private:
	shared_ptr<AVLNode<T>> insert_with_root(shared_ptr<AVLNode<T>> root, int key, T data, shared_ptr<AVLNode<T>> father)
	{
		if (root == nullptr)
		{
			root = shared_ptr<AVLNode<T>>(new AVLNode<T>(key, data, father));
			return root;
		}
		if (key > root->key)
		{
			root->right = insert_with_root(root->right, key, data, root);
			root = balanceTree(root);
		}
		else
		{
			root->left = insert_with_root(root->left, key, data, root);
			root = balanceTree(root);
		}
		return root;
	}

public:

	shared_ptr<AVLNode<T>> root;

	AVLTree(shared_ptr<AVLNode<T>> root = nullptr) : root{ root } {}

	shared_ptr<AVLNode<T>> find(shared_ptr<AVLNode<T>> curr_node, const int& key)
	{
		if (curr_node == nullptr || curr_node->key == key)
		{
			return curr_node;
		}
		if (curr_node->key < key)
		{
			return find(curr_node->right, key);
		}
		return find(curr_node->left, key);
	}

	int getBalanceFactor(shared_ptr<AVLNode<T>> node)
	{
		return getHeight(node->left) - getHeight(node->right);
	}

	void insert(const int& key, const T& data)
	{
		this->root = insert_with_root(this->root, key, data, this->root);
	}

	shared_ptr<AVLNode<T>> handleZeroSonsRemove(shared_ptr<AVLNode<T>> node)
	{
		if (node->father->left == node)
		{
			node->father->left = nullptr;
		}
		else
		{
			node->father->right = nullptr;
		}
		return node->father;
	}
	shared_ptr<AVLNode<T>> handleOneSonRemove(shared_ptr<AVLNode<T>> node)
	{
		shared_ptr<AVLNode<T>> node_location;
		shared_ptr<AVLNode<T>> node_son_location = node->left == nullptr ? node->right : node->left;
		shared_ptr<AVLNode<T>> father = node->father;
		if (node->father->right == node)
		{
			father->right = node_son_location;
		}
		else
		{
			father->left = node_son_location;
		}
		return father;
	}
	shared_ptr<AVLNode<T>> handleTwoSonsRemove(shared_ptr<AVLNode<T>> node)
	{
		shared_ptr<AVLNode<T>> tmp = findPrecedingNode(node);
		int temp = tmp->key;
		tmp->key = node->key;
		node->key = temp;
		temp = tmp->data;
		tmp->data = node->data;
		node->data = temp;
		//std::swap(node->key, tmp->key);
		//std::swap(node->data, tmp->data);

		bool left_son_exist = tmp->left != nullptr;
		bool right_son_exist = tmp->right != nullptr;

		shared_ptr<AVLNode<T>> father;
		if (!left_son_exist && !right_son_exist)
		{
			father = handleZeroSonsRemove(tmp);
		}
		else
		{
			father = handleOneSonRemove(tmp);
		}
		return father;
	}
	std::shared_ptr<AVLNode<T>> findPrecedingNode(std::shared_ptr<AVLNode<T>> node)
	{
		std::shared_ptr<AVLNode<T>> tmp = node->right;
		while (tmp->left != nullptr)
		{
			tmp = tmp->left;
		}
		return tmp;
	}
	shared_ptr<AVLNode<T>> removeFromBST(shared_ptr<AVLNode<T>> node, bool left_son_exist, bool right_son_exist)
	{
		shared_ptr<AVLNode<T>> father;
		if (!left_son_exist && !right_son_exist)
		{
			father = handleZeroSonsRemove(node);
		}
		else if (left_son_exist && right_son_exist)
		{
			father = handleTwoSonsRemove(node);
		}
		else
		{
			father = handleOneSonRemove(node);
		}
		return father;
	}
	void remove(const int& key)
	{
		shared_ptr<AVLNode<T>> node = find(root, key);
		if (node == nullptr)
		{
			throw Failure();
		}
		bool left_son_exist = node->left != nullptr;
		bool right_son_exist = node->right != nullptr;
		shared_ptr<AVLNode<T>> father = removeFromBST(node, left_son_exist, right_son_exist);
		balance(father);
	}

	void balance(shared_ptr<AVLNode<T>> node)
	{
		while (node != this->root)
		{
			node = balanceTree(node);
			node = node->father;
		}
		this->root = balanceTree(this->root);
	}

	shared_ptr<AVLNode<T>> balanceTree(shared_ptr<AVLNode<T>> root)
	{
		int balanace_factor = getBalanceFactor(root);
		if (balanace_factor == 2)//too big in left
		{
			root = getBalanceFactor(root->left) > 0 ? left_left_rotation(root) : left_right_rotation(root);
		}
		else if (balanace_factor == -2)//too big in right
		{
			root = getBalanceFactor(root->right) > 0 ? right_left_rotation(root) : right_right_rotation(root);
		}
		return root;
	}

	shared_ptr<AVLNode<T>> left_left_rotation(shared_ptr<AVLNode<T>> root)
	{
		shared_ptr<AVLNode<T>> x = root->left;
		x->father = root->father;
		shared_ptr<AVLNode<T>> T2 = x->right;

		// Perform rotation  
		x->right = root;
		root->father = x;
		root->left = T2;
		if (T2 != nullptr)
		{
			T2->father = root;
		}
		return x;
	}
	shared_ptr<AVLNode<T>> left_right_rotation(shared_ptr<AVLNode<T>> root)
	{
		//shared_ptr<AVLNode<T>> node;
		//node = root->left;
		root->left = right_right_rotation(root->left);
		root = left_left_rotation(root);
		return root;
	}

	shared_ptr<AVLNode<T>> right_right_rotation(shared_ptr<AVLNode<T>> root)
	{
		shared_ptr<AVLNode<T>> y = root->right;
		y->father = root->father;
		shared_ptr<AVLNode<T>> T2 = y->left;

		// Perform rotation  
		y->left = root;
		root->father = y;
		root->right = T2;
		if (T2 != nullptr)
		{
			T2->father = root;
		}
		return y;
	}

	shared_ptr<AVLNode<T>> right_left_rotation(shared_ptr<AVLNode<T>> root)
	{
		root->right = left_left_rotation(root->right);
		root = right_right_rotation(root);
		return root;
	}

	int getHeight(shared_ptr<AVLNode<T>> node)
	{
		int height = -1;
		if (node != nullptr)
		{
			return 1 + std::max(getHeight(node->left), getHeight(node->right));
		}
		return height;
	}

	void printTree(shared_ptr<AVLNode<T>> node)
	{
		if (node != NULL)
		{
			printTree(node->left);
			std::cout << node->key << " " << "BF: " << getBalanceFactor(node) << " Height: " << getHeight(node) << std::endl;
			printTree(node->right);
		}
	}

	void treeClear()
	{
		root = nullptr;
	}
};

#endif